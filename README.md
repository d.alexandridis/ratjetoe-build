# Ratjetoe Build
Deze repository bevat de laatste versie van Ratjetoe - Ruby's Missie. Binnen de repository bevinden zich verschillende bestanden. De bestanden beginnen allemaal met <i>"Rubys Missie"</i> gevolgd bij het platform waarvoor het spel is gebuild (Windows of MacOS). 

Vervolgens kan het zijn dat er per platform meerdere type bestanden te vinden zijn. De bestanden die eindigen in ".zip" zijn te openen op elke Windows of MacOS computer. Om de bestanden uit te kunnen pakken met de ".7z" extensie is het programma [7-Zip](https://www.7-zip.org/) vereist. Deze bestanden zijn over het algemeen kleiner en daardoor sneller te downloaden. 

Wanneer je niet zeker weet welk bestand je moet hebben, kies dan het bestand met de ".zip" extensie.

## Downloaden en gebruiken

### Windows Instructies

<b>Downloaden</b>

Om de applicatie voor op een Windows computer te gebruiken, download dan één van de bestanden met <i>- Windows</i> in de naam: 

![Windows builds](/images/windows-builds.png "Windows builds")

Klik op het bestand met de gewenste extensie (in geval van twijfel, kies dan voor het .zip bestand). Klik vervolgens op de download-knop in de rechterbovenhoek op het scherm:

![Download knop](/images/download-knop.png "Download knop")

<b>Bestanden uitpakken</b>

Wacht totdat de bestanden zijn gedownload en ga vervolgens naar de download locatie op uw computer (op Windows is dit vaak de map genaamd "Downloads"). Zoek het bestand dat u heeft gedownload - bijvoorbeeld Rubys Missie - Windows.zip. Pak het bestand vervolgens uit door met het juiste programma. 

Wanneer u het bestand met de .zip extensie heeft gekozen dan doet u dit door op het bestand te klikken met de <b>rechter muisknop</b>. Vervolgens klikt u op "alles uitpakken":

![Alles uitpakken](/images/windows-uitpakken.png "Alles uitakken")

U ziet vervolgens een scherm waarbij u wordt gevraagd om een map te selecteren om de bestanden in te plaatsen. Deze kunt u aanpassen naar de gewenste locatie. Wanneer u deze niet aanpassen, dan worden de bestanden uitgepakt in dezelfde map als waar de download staat. Klik vervolgens op de knop "Uitpakken".

![Knop uitpakken](/images/windows-knop-uitpakken.png "Knop uitpakken")

Wacht tot alle bestanden zijn uitgepakt.

<b> Spel spelen! </b>

Ga naar de map waarin u de bestanden heeft uitgepakt. Hierin vindt u het bestand genaamd "Rubys Missie". Dubbelklik op dit bestand om het spel te starten!

![Windows spel starten](/images/windows-starten.png "Spel starten")


### MacOS Instructies

Deze instructies volgen binnenkort...




